@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Регистрация</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('register') }}" class="js_register" novalidate>
                            @csrf
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Имя</label>
                                <div class="col-md-6">
                                    <input id="name"
                                           type="text"
                                           class="form-control"
                                           name="name"
                                           value="{{ old('name') }}"
                                           autofocus>
                                    <span class="invalid-feedback j-name"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>
                                <div class="col-md-6">
                                    <input id="email"
                                           type="email"
                                           class="form-control"
                                           name="email"
                                           value="{{ old('email') }}" >
                                    <span class="invalid-feedback j-email"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="region" class="col-md-4 col-form-label text-md-right">Область</label>
                                <div class="col-md-6">
                                    <select name="region_id" id="region" class="form-control" >
                                        <option value="">Выберите область</option>
                                        @foreach($regions as $id => $region)
                                            <option value="{{ $id }}">{{ $region }}</option>
                                        @endforeach
                                    </select>
                                    <span class="invalid-feedback j-region"></span>
                                </div>
                            </div>
                            <div class="form-group row" style="display: none;">
                                <label for="city" class="col-md-4 col-form-label text-md-right">Город</label>
                                <div class="col-md-6">
                                    <select name="city_id" id="city" class="form-control" required>
                                        <option value="">Выберите город</option>
                                    </select>
                                    <span class="invalid-feedback j-city"></span>
                                </div>
                            </div>
                            <div class="form-group row" style="display: none;">
                                <label for="area" class="col-md-4 col-form-label text-md-right">Регион</label>
                                <div class="col-md-6">
                                    <select name="area_id" id="area" class="form-control" required>
                                        <option value="">Выберите район</option>
                                    </select>
                                    <span class="invalid-feedback j-area"></span>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">Зарегистрироваться</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
