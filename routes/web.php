<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/register/cities/{regionId}', 'Auth\RegisterController@getCities');
Route::get('/register/areas/{cityId}', 'Auth\RegisterController@getAreas');
