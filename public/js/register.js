class Register {
    constructor() {
        this.bindEvents();
    }

    bindEvents() {
        $(document)
            .on('submit', '.js_register', function (event) {
                event.preventDefault();

                const $this = $(this);

                $.post('/register', $this.serialize())
                    .done(function (response) {
                        window.location = response.url;
                    })
                    .fail(function (response) {
                        let errorsObject = response.responseJSON ? response.responseJSON.errors : false,
                            nameError = $('.j-name'),
                            emailError = $('.j-email'),
                            regionError = $('.j-region'),
                            cityError = $('.j-city'),
                            areaError = $('.j-area');

                        if (errorsObject['name']) {
                            nameError.html(errorsObject['name'] + '<br>').show();
                        } else {
                            nameError.html('').hide();
                        }

                        if (errorsObject['email']) {
                            emailError.html(errorsObject['email'] + '<br>').show();
                        } else {
                            emailError.html('').hide();
                        }

                        if (errorsObject['region_id']) {
                            regionError.html(errorsObject['region_id'] + '<br>').show();
                        } else {
                            regionError.html('').hide();
                        }

                        if (errorsObject['city_id']) {
                            cityError.html(errorsObject['city_id'] + '<br>').show();
                        } else {
                            cityError.html('').hide();
                        }

                        if (errorsObject['area_id']) {
                            areaError.html(errorsObject['area_id'] + '<br>').show();
                        } else {
                            areaError.html('').hide();
                        }
                    });
            })
            .on('change', '#region', function () {
                $.get('/register/cities/' + $(this).val())
                    .done(function (response) {
                        $.each(response.cities, function (key, value) {
                            $('#city')
                                .append($("<option></option>")
                                    .attr("value", key)
                                    .text(value));
                        });
                        $('#city').closest('.row').show();
                    });
            })
            .on('change', '#city', function () {
                $.get('/register/areas/' + $(this).val())
                    .done(function (response) {
                        $.each(response.areas, function (key, value) {
                            $('#area')
                                .append($("<option></option>")
                                    .attr("value", key)
                                    .text(value));
                        });
                        $('#area').closest('.row').show();
                    });
            });
    }
}

$(() => {
    new Register();
})