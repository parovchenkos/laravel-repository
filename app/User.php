<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property string $name
 * @property string $email
 * @property string $region_id
 * @property string $city_id
 * @property string $area_id
 *
 * Class User
 * @package App
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'area_id'
    ];

    public function address()
    {
        return $this->hasOne('App\Territory', 'ter_id', 'area_id');
    }
}
