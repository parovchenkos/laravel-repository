<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Territory
 * @package App
 */
class Territory extends Model
{
    /**
     * @var string
     */
    protected $table = 't_koatuu_tree';

    const TYPE_REGION = 0;
    const TYPE_CITY = 1;
    const TYPE_AREA = 2;
}
