<?php

namespace App\Http\Controllers\Auth;

use App\Territory;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Validator;
use Illuminate\Support\Facades\Validator as FacadeValidator;
use Illuminate\View\View;

/**
 * Class RegisterController
 * @package App\Http\Controllers\Auth
 */
class RegisterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data): Validator
    {
        return FacadeValidator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:users',
            'region_id' => 'required|string',
            'city_id' => 'required|string',
            'area_id' => 'required|string',
        ], $this->getValidationMessages());
    }

    /**
     * @return array
     */
    protected function getValidationMessages(): array
    {
        return [
            'name.required' => 'Заполните поле Имя',
            'name.max' => 'Имя не может превышать 255 символов',
            'email.required' => 'Заполните поле Email',
            'email.email' => 'Введите корректный Email',
            'email.unique' => 'Этот Email уже используется',
            'region_id.required' => 'Выберите область',
            'city_id.required' => 'Выберите город',
            'area_id.required' => 'Выберите район',
        ];
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return User
     */
    protected function create(array $data): User
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'area_id' => $data['area_id']
        ]);
    }

    /**
     * Show the application registration form.
     *
     * @return View
     */
    public function showRegistrationForm(): View
    {
        $territories = Territory::where('ter_type_id', Territory::TYPE_REGION)->get()->toArray();
        $regions = array_column($territories, 'ter_name', 'ter_id');

        return view('auth.register', [
            'regions' => $regions
        ]);
    }

    /**
     * @param Request $request
     * @return array
     * @throws ValidationException
     */
    public function register(Request $request): array
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        Auth::guard()->login($user);

        return ['url' => route('home')];
    }

    /**
     * @param string $regionId
     * @return array
     */
    public function getCities(string $regionId): array
    {
        $territories = Territory::where('ter_pid', 'like', $regionId)
            ->where('ter_type_id', Territory::TYPE_CITY)
            ->get()
            ->toArray();

        return ['cities' => array_column($territories, 'ter_name', 'ter_id')];
    }

    /**
     * @param string $cityId
     * @return array
     */
    public function getAreas(string $cityId): array
    {
        $townId = Territory::where('ter_id', 'like', $cityId)
            ->where('ter_type_id', Territory::TYPE_CITY)
            ->where('ter_level', 2)
            ->where('ter_mask', 5)
            ->pluck('ter_pid')[0];
        $territories = Territory::where('ter_pid', 'like', $townId)
            ->where('ter_type_id', Territory::TYPE_AREA)
            ->get()
            ->toArray();

        return ['areas' => array_column($territories, 'ter_name', 'ter_id')];
    }
}
